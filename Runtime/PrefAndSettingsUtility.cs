using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{
    public static class PrefAndSettingsUtility
    {
#if UNITY_EDITOR
        /// <summary>
        /// Mimmic the behaviour of the inspector, but skip the 'Script' field and adjust
        /// all label prefix width to match the longest
        /// </summary>
        /// <param name="serializedObject"></param>
        public static void DefaultGUI(SerializedObject serializedObject)
        {
            SerializedProperty field = GetFirstProperty(serializedObject);

            float maxWidth = 100;
            while (field.NextVisible(false))
            {
                float w = GUI.skin.label.CalcSize(new GUIContent(field.displayName)).x;
//                Debug.Log(field.displayName + ":" + w);
                maxWidth = Mathf.Max(maxWidth, w);
            }
            float prevLabelWidth = SetLabelWidth(maxWidth);

            field = GetFirstProperty(serializedObject);
            while (field.NextVisible(false))
                EditorGUILayout.PropertyField(field, true);

            SetLabelWidth(prevLabelWidth);
        }


        /// <summary>
        /// Set EditorGUIUtility.labelWidth and returns its previous value 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static float SetLabelWidth(float val)
        {
            float prev = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = val;
            return prev;
        }


        /// <summary>
        /// Skip the 'Script' property an returns the next visible property
        /// </summary>
        /// <param name="serializedObject"></param>
        /// <returns></returns>
        public static SerializedProperty GetFirstProperty(SerializedObject serializedObject)
        {
            SerializedProperty field = serializedObject.GetIterator();
            //Skip the Script field
            field.NextVisible(true);
            return field;
        }
#endif
    }
}
