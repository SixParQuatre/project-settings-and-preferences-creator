﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{
    public class SOProjectSettings : ScriptableObject
    {
        const string resourcefolder = "Assets/Resources/";
        const string subFolder = "Settings/";
        const string completePath = resourcefolder + subFolder;
        public static T GetOrCreateAsset<T>() where T : SOProjectSettings
        {
            string className = typeof(T).Name;
            string filename = className + ".asset";
            string pathInResources = subFolder + className;
            T settings = Resources.Load<T>(pathInResources);

            string creationPath = completePath + filename;

#if UNITY_EDITOR
            if (settings == null)
            {
                if (!Directory.Exists(completePath))
                {
                    Directory.CreateDirectory(completePath);
                }
                settings = ScriptableObject.CreateInstance(typeof(T).Name) as T;
                AssetDatabase.CreateAsset(settings, creationPath);
                AssetDatabase.SaveAssets();
            }
#else
            if (settings == null)
            {
                Debug.LogError($"Couldnt find settings for {className}; it must be located at {creationPath}." +
                    $"Will create a temporary one to try and avoid complete breakdown.");
                settings = ScriptableObject.CreateInstance(typeof(T).Name) as T;
            }
#endif

            return settings;
        }

#if UNITY_EDITOR
        /// <summary>
        /// Mimmic the behaviour of the inspector. Override to do your own UI
        /// </summary>
        /// <param name="serializedObject"></param>
        protected virtual void OnGUI_Internal(SerializedObject serializedObject)
            => PrefAndSettingsUtility.DefaultGUI(serializedObject);
#endif

#if UNITY_EDITOR
        public static SettingsProvider CreateGenericProvider(System.Type type, string _path, Func<SOProjectSettings> SettingsObjectProvider)
        {
            var ourKeyWords = new HashSet<string>();
            foreach (FieldInfo field in type.GetFields())
                ourKeyWords.Add(field.Name);

            string _label = _path;
            int lastSlash = _label.LastIndexOf("/");
            if (lastSlash != -1)
                _label = _label.Substring(lastSlash + 1);


            // First parameter is the path in the Settings window.
            // Second parameter is the scope of this setting: it only appears in the Project Settings window.
            var provider = new SettingsProvider("Project/" + _path, SettingsScope.Project)
            {
                // By default the last token of the path is used as display name if no label is provided.
                label = _label,
                // Create the SettingsProvider and initialize its drawing (IMGUI) function in place:
                guiHandler = (searchContext) =>
                {

                    SOProjectSettings settings = SettingsObjectProvider.Invoke();
                    var serializedObject = new SerializedObject(settings);
                    serializedObject.Update();

                    settings.OnGUI_Internal(serializedObject);

                    serializedObject.ApplyModifiedProperties();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.FlexibleSpace();
                    EditorGUI.BeginDisabledGroup(!EditorUtility.IsDirty(serializedObject.targetObject));
                    if (GUILayout.Button("Save"))
                    {
                        AssetDatabase.SaveAssets(); 
                    }
                    EditorGUI.EndDisabledGroup();
                    EditorGUILayout.EndHorizontal();

                },

                // Populate the search keywords to enable smart search filtering and label highlighting:
                keywords = ourKeyWords
            };

            return provider;
        }
#endif

    }
}