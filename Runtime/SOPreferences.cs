﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{
    public class SOPreferences : ScriptableObject
    {

#if UNITY_EDITOR

        /// <summary>
        /// Mimmic the behaviour of the inspector. Override to do your own UI
        /// </summary>
        /// <param name="serializedObject"></param>
        protected virtual void OnGUI_Internal(SerializedObject serializedObject)
            => PrefAndSettingsUtility.DefaultGUI(serializedObject);


        protected virtual void OnChanged()
        {
            foreach (EditorWindow window in Resources.FindObjectsOfTypeAll<EditorWindow>())
                window.Repaint();
        }

        public static SettingsProvider CreateGenericProvider(System.Type type, string _path, Func<SOPreferences> PreferenceObjectProvider)
        {
            var ourKeyWords = new HashSet<string>();
            foreach (FieldInfo field in type.GetFields())
                ourKeyWords.Add(field.Name);

            string _label = _path;
            int lastSlash = _label.LastIndexOf("/");
            if (lastSlash != -1)
                _label = _label.Substring(lastSlash + 1);

            // First parameter is the path in the Settings window.
            // Second parameter is the scope of this setting: it only appears in the Preferences window.
            var provider = new SettingsProvider("Preferences/"+_path, SettingsScope.User)
            {

                // By default the last token of the path is used as display name if no label is provided.
                label = _label,
                // Create the SettingsProvider and initialize its drawing (IMGUI) function in place:
                guiHandler = (searchContext) =>
                {
                    SOPreferences pref = PreferenceObjectProvider.Invoke();
                    
                    SerializedObject serializedObject = new SerializedObject(pref);
                    serializedObject.Update();
                    pref.OnGUI_Internal(serializedObject);
                    serializedObject.ApplyModifiedProperties();

                    if (GUI.changed)
                    {
                        string className = pref.GetType().Name;
                        SaveToPlayerPref(serializedObject, className);
                        pref.OnChanged();
                    }

                },


                // Populate the search keywords to enable smart search filtering and label highlighting:
                keywords = ourKeyWords
            };

            return provider;
        }

        protected static void LoadFromEditorPrefs<T>(out T currentOption, string prefix) where T : ScriptableObject
        {
            currentOption = ScriptableObject.CreateInstance<T>();
            SerializedObject optionObj = new SerializedObject(currentOption);

            SerializedProperty field = optionObj.GetIterator();
            field.NextVisible(true);

            while (field.NextVisible(false))
            {
                string prefKey = prefix + "_" + field.name;
                switch (field.propertyType)
                {
                    case SerializedPropertyType.Integer:
                        field.intValue = EditorPrefs.GetInt(prefKey, field.intValue);
                        break;
                    case SerializedPropertyType.Float:
                        field.floatValue = PlayerPrefs.GetFloat(prefKey, field.floatValue);
                        break;
                    case SerializedPropertyType.Boolean:
                        field.boolValue = EditorPrefs.GetBool(prefKey, field.boolValue);
                        break;
                    case SerializedPropertyType.String:
                        field.stringValue = EditorPrefs.GetString(prefKey, field.stringValue);
                        break;
                    case SerializedPropertyType.Enum:
                        field.enumValueIndex = EditorPrefs.GetInt(prefKey, field.enumValueIndex);
                        break;
                    case SerializedPropertyType.Color:
                        field.colorValue = GetVector4(prefKey, field.colorValue);
                        break;
                    case SerializedPropertyType.Vector4:
                        field.vector4Value = GetVector4(prefKey, field.vector4Value);
                        break;
                    default:
                        LogErrorType(field);
                        break;
                }
            }
            optionObj.ApplyModifiedProperties();
        }

        static void SaveToPlayerPref(SerializedObject optionObj, string prefix)
        {
            SerializedProperty field = optionObj.GetIterator();
            field.NextVisible(true);

            while (field.NextVisible(false))
            {
                string prefKey = prefix + "_" + field.name;
                switch (field.propertyType)
                {
                    case SerializedPropertyType.Integer:
                        EditorPrefs.SetInt(prefKey, field.intValue);
                        break;
                    case SerializedPropertyType.Float:
                        EditorPrefs.SetFloat(prefKey, field.floatValue);
                        break;
                    case SerializedPropertyType.Boolean:
                        EditorPrefs.SetBool(prefKey, field.boolValue);
                        break;
                    case SerializedPropertyType.String:
                        EditorPrefs.SetString(prefKey, field.stringValue);
                        break;
                    case SerializedPropertyType.Enum:
                        EditorPrefs.SetInt(prefKey, field.enumValueIndex);
                        break;
                    case SerializedPropertyType.Color:
                        SetVector4(prefKey, field.colorValue);
                        break;
                    case SerializedPropertyType.Vector4:
                        SetVector4(prefKey, field.vector4Value);
                        break;
                    default:
                        LogErrorType(field);
                        break;
                }
            }
        }

        static void SetVector4(string key, Vector4 val)
        {
            EditorPrefs.SetFloat(key + ".w", val.w);
            EditorPrefs.SetFloat(key + ".x", val.x);
            EditorPrefs.SetFloat(key + ".y", val.y);
            EditorPrefs.SetFloat(key + ".z", val.z);
        }

        static Vector4 GetVector4(string key, Vector4 defaultVal)
        {
            Vector4 val = defaultVal;
            val.w = EditorPrefs.GetFloat(key + ".w", val.w);
            val.x = EditorPrefs.GetFloat(key + ".x", val.x);
            val.y = EditorPrefs.GetFloat(key + ".y", val.y);
            val.z = EditorPrefs.GetFloat(key + ".z", val.z);
            return val;
        }

        static void LogErrorType(SerializedProperty field)
        {
            Debug.LogError($"ScriptableObject->EditorPrefsExt does not support the type of field {field.name} [{field.GetType()}]");
        }


#endif

    }
}