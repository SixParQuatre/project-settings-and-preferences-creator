﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace SixParQuatre
{
    public class ProjectSettingAndPreferencesCreator : EditorWindow
    {
        static ProjectSettingAndPreferencesCreator window = null;
        [MenuItem("Assets/Create/C# Advanced/Project Settings", false, 81)]
        static void Init_ProjectSettings() => Init(SettingsScope.Project);

        [MenuItem("Assets/Create/C# Advanced/Project Settings", true, priority = 81)]
        static bool Init_ProjectSettings_Valid() => window == null;

        [MenuItem("Assets/Create/C# Advanced/Editor Preferences", false, 81)]
        static void Init_Preferences() => Init(SettingsScope.User);

        [MenuItem("Assets/Create/C# Advanced/Editor Preferences", true, priority = 81)]
        static bool Init_Preferences_Valid() => window == null;

        static string projectSettingsTemplatePath = null;
        static string preferencesTemplatePath = null;
        static void Init(SettingsScope scope)
        {
            if (window != null)
                return;

            window = ScriptableObject.CreateInstance<ProjectSettingAndPreferencesCreator>();
            if (scope == SettingsScope.Project)
            {
                window.titleContent.text = "Create Project Settings";
            }
            else
            {
                window.titleContent.text = "Create Project Preferences";
            }
            currentName = "";
            window.firstRun = true;
            window.scope = scope;

            window.ShowUtility();
        }

        static Vector2 size = new Vector2(300, 130);
        bool firstRun = true;
        static string currentName;
        SettingsScope scope;
        static string firstFieldName = "NameField";
        string aNamespace = "";
        string label = "";
        string UIPath = "";

        static float indentSize = 15;
        int indent = 0;
        void OnGUI()
        {
            maxSize = size;
            minSize = size;
            indent = 0;
            EditorGUILayout.Separator();

            string type = scope == SettingsScope.Project ? "project settings" : "preferences";

            // The base name of the object
            StringField("Name", ref currentName, $"Name of the file and class. Spaces will be replaced by underscores.", firstFieldName);
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(indent * indentSize));
            GUILayout.Label("Optional:");
            GUILayout.EndHorizontal();
            indent++;
            StringField("Path", ref UIPath, $"Path of these {type} in the {type} window. Each level is separated by a '/'.\n\ne.g. MyStudioName/LevelDesign\n\nIf left empty, {type} will be at the root.");
            StringField("Namespace", ref aNamespace, $"Namespace of the class. Spaces will be replaced by underscores.\n If left empty class will be in no namespace"); 
            StringField("Label", ref label, $"Label of these {type} in the {type} windows. Should similar to the value in Name, but may include spaces.\nIf left empty, the Name will be used");
            indent--;
            //The buttons
            GUILayout.BeginHorizontal();
            GUILayout.Label("");
            EditorGUI.BeginDisabledGroup(NoContent(currentName));
            if (GUILayout.Button("OK", GUILayout.Width(50)) || Event.current.keyCode == KeyCode.KeypadEnter || Event.current.keyCode == KeyCode.Return)
            {
                CreateFile(currentName);
                window = null;
                this.Close();
            }
            EditorGUI.EndDisabledGroup();

            if (GUILayout.Button("Cancel", GUILayout.Width(50)) || Event.current.keyCode == KeyCode.Escape)
            {
                window = null;

                this.Close();
            }
            GUILayout.EndHorizontal();

            //We need this because setting the position on click doesn't seem to work reliably.
            if (firstRun)
            {
                //This is weird, but it works better than using the opposite.
                if (!Event.current.isMouse)
                {
                    Rect pos = position;
                    pos.position = GUIUtility.GUIToScreenPoint(Event.current.mousePosition);
                    position = pos;
                    GUI.FocusControl(firstFieldName);
                    firstRun = false;
                }
            }
        }

        bool NoContent(string input)
        {
            return string.IsNullOrWhiteSpace(input) || string.IsNullOrEmpty(input);
        }

        static float labelWidth = 80;
        void StringField(string label, ref string currentValue, string tooltip, string controlName = null)
        {
            GUIContent lbl = new GUIContent(label, tooltip);
            GUILayout.BeginHorizontal();
            GUILayout.Label("", GUILayout.Width(indent * indentSize));
            GUILayout.Label(lbl, GUILayout.Width(labelWidth));
            if(!string.IsNullOrEmpty(controlName))
                GUI.SetNextControlName(controlName);
            currentValue = GUILayout.TextField(currentValue);
            GUIContent obviousTooltip = EditorGUIUtility.IconContent("_Help");
            obviousTooltip.tooltip = tooltip;
            GUILayout.Label(obviousTooltip, GUILayout.Width(20));
            GUILayout.EndHorizontal();
        }

        void CreateFile(string name)
        {
            RefreshTemplates();
            string root = null;
            if (Selection.activeObject != null)
            {
                string path = AssetDatabase.GetAssetPath(Selection.activeObject);
                bool isFolder = !Path.HasExtension(path) && AssetDatabase.IsValidFolder(path);
                if (isFolder)
                    root = path;
                else
                    root = Path.GetDirectoryName(AssetDatabase.GetAssetPath(Selection.activeObject));
            }
            if (root == null)
            {
                EditorUtility.DisplayDialog("Can't create Files", "Couldn't find the folder for the sources.\nClear your search field in the Project window and retry.", "OK");
                return;
            }
            if (!Directory.Exists(root))
                root = root.Substring(0, root.LastIndexOf('/'));

            //1.Create the action script
            string templatePath = null;
            if (scope == SettingsScope.Project)
                templatePath += projectSettingsTemplatePath;
            else
                templatePath += preferencesTemplatePath;

            name = name.Trim();

            if (NoContent(label))
                label = name; 
            
            name = name.Replace(" ", "_");

            aNamespace = aNamespace.Trim();
            aNamespace.Replace(" ", "_");


            if (!string.IsNullOrEmpty(UIPath) && !UIPath.EndsWith("/"))
                UIPath += "/";

            var script = File.ReadAllText(templatePath);
            script = script.Replace("[{CLASS_NAME}]", name);
            script = script.Replace("[{SETTINGS_PATH}]", UIPath + label);

            if (NoContent(aNamespace))
            {
                script = script.Replace("namespace [{NAMESPACE}]", "");
                script = script.Remove(script.IndexOf('{'), 1);
                script = script.Remove(script.LastIndexOf('}'), 1);
                script = script.Replace("\n    ", "");
            }
            else
                script = script.Replace("[{NAMESPACE}]", aNamespace);

            if (!Directory.Exists(root))
            {
                Directory.CreateDirectory(root);
            }
            var scriptPath = root + "/" + name + ".cs";
            if (File.Exists(scriptPath))
            {
                Debug.Log("ERROR: " + scriptPath + " already exists.");
                return;
            }
            File.WriteAllText(scriptPath, script);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            MonoScript asset = AssetDatabase.LoadAssetAtPath<MonoScript>(scriptPath);
            AssetDatabase.OpenAsset(asset, 1);
        }

        void RefreshTemplates()
        {
            if (projectSettingsTemplatePath == null)
                projectSettingsTemplatePath = FindPathForTemplate("SOProjectSettings");

            if (preferencesTemplatePath == null)
                preferencesTemplatePath = FindPathForTemplate("SOPreferences");
        }

        /// <summary>
        /// Function to find a a cstemplate file in the project not matter how this code was imported (git package, copy paste, etc...)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        static string FindPathForTemplate(string name)
        {
            string filename = name + ".cstemplate";
            foreach (string guid in AssetDatabase.FindAssets($"t:DefaultAsset {name}"))
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                if (path.EndsWith(filename))
                    return path;
            }
            return null;
        }

    }
}
